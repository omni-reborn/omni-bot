# --- Base node ---
FROM node:alpine as base
RUN apk add --no-cache --virtual .gyp python make g++
WORKDIR /usr/src/app
COPY package.json .

# --- Dependencies ---
FROM base AS dependencies
RUN yarn install --no-progress
RUN apk del .gyp

# --- Build ---
FROM dependencies AS build
COPY src ./src
COPY config ./config
COPY tsconfig.json .
COPY gulpfile.js .
RUN yarn run build

# --- Release ---
FROM base AS release
COPY --from=dependencies /usr/src/app/node_modules ./node_modules
COPY --from=build /usr/src/app/build/src ./build
COPY config ./config

EXPOSE $APP_PORT

# --- Entrypoint command ---
CMD yarn run start