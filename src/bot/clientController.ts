import { Client as DiscordClient, PresenceData, ClientEvents } from 'discord.js';
import { injectable, inject } from 'inversify';
import { ILogger } from 'src/logger';
import { SERVICE_IDENTIFIER } from 'src/constants';
import { DiscordClientError } from 'src/error/bot';

export interface IDiscordClientController {
  login(token: string): Promise<void>;
  setPresence(data: PresenceData): Promise<void>;
  attachHandler(event: keyof ClientEvents, handler: (...args: ClientEvents[keyof ClientEvents]) => void): void;
  removeAllListeners(events: Array<keyof ClientEvents>): void;
  destroy(): void;
}

@injectable()
export class DisordClientController implements IDiscordClientController {
  private readonly client: DiscordClient;
  private readonly logger: ILogger;

  public constructor(
    @inject(SERVICE_IDENTIFIER.DiscordClient) client: DiscordClient,
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
  ) {
    this.client = client;
    this.logger = logger;
  }

  /**
   * Attempts to log into the discord api as a bot service.
   * @param token discord bot token
   */
  public async login(token: string) {
    try {
      this.logger.info('Logging into the discord API.');
      await this.client.login(token);
    } catch (error) {
      this.logger.error('Failed to log in to the discord API.', { error });
      throw new DiscordClientError('Failed to log in to the discord API.');
    }
  }

  /**
   * Attempts to set bot user presence.
   * @param data presence data
   */
  public async setPresence(data: PresenceData) {
    try {
      this.logger.info('Setting bot presence.');
      await this.client.user?.setPresence(data);
      this.logger.info('Bot presence set successfully.');
    } catch (error) {
      this.logger.error('Failed to set bot presence.', { error });
      throw new DiscordClientError('Failed to set bot presence.');
    }
  }

  /**
   * Attaches a handler to a bot event.
   * @param event bot event
   * @param handler event handler
   */
  public attachHandler(event: keyof ClientEvents, handler: (...args: ClientEvents[keyof ClientEvents]) => void) {
    this.logger.info(`Attaching event handler for ${event}`);
    this.client.on(event, handler);
  }

  /**
   * Removes all listeners given event names.
   * @param events event names
   */
  public removeAllListeners(events: Array<keyof ClientEvents>): void {
    for (const event of events) {
      this.logger.info(`Removing event handler for ${event}`);
      this.client.removeAllListeners(event);
    }
  }

  /**
   * Attempts to destroy the bot service.
   */
  public destroy() {
    try {
      this.logger.info('Destroying client service.');
      this.client.destroy();
    } catch (error) {
      this.logger.error('Failed to destroy bot client service.');
      throw new DiscordClientError('Could not destroy bot client.');
    }
  }
}

