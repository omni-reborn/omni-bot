import { injectable, inject, multiInject } from 'inversify';
import { ILogger } from 'src/logger';
import { SERVICE_IDENTIFIER, CONFIG_IDENTIFIER, Command } from 'src/constants';
import { Subject } from 'rxjs';
import { Message as DiscordMessage } from 'discord.js';
import { IDiscordClientController } from './clientController';
import { IBotConfig } from 'src/config/bot';
import { IFilter } from 'src/service/filter/baseFilter';
import { IMatcher } from 'src/service/matcher';
import { MatchedMessage } from './matchedMessage';
import { IHandler } from 'src/service/handler';

export interface IBot {
  start(): Promise<void>;
}

/**
 * Core bot class, representing the bot service.
 * Handles subjects of messages and processing-discordclient interactions.
 *
 * @export
 * @class Bot
 * @implements {IBot}
 */
@injectable()
export class Bot implements IBot {
  private readonly logger: ILogger;
  private readonly config: IBotConfig;
  private readonly clientController: IDiscordClientController;

  private readonly matcher: IMatcher;
  private readonly filters: Array<IFilter>;
  private readonly handlers: Array<IHandler>;

  private readonly incoming: Subject<DiscordMessage>;
  private readonly filtered: Subject<DiscordMessage>;
  private readonly matched: Subject<MatchedMessage>;

  public constructor(
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
    @inject(CONFIG_IDENTIFIER.IBotConfig) config: IBotConfig,
    @inject(SERVICE_IDENTIFIER.IDiscordClientController) clientController: IDiscordClientController,
    @inject(SERVICE_IDENTIFIER.IMatcher) matcher: IMatcher,
    @multiInject(SERVICE_IDENTIFIER.IFilter) filters: Array<IFilter>,
    @multiInject(SERVICE_IDENTIFIER.IHandler) handlers: Array<IHandler>,
  ) {
    this.logger = logger;
    this.config = config;
    this.clientController = clientController;

    this.matcher = matcher;
    this.filters = filters;
    this.handlers = handlers;
  }

  /**
   * Handle messages that have been produced by a discord client and pass them through
   * available filters. When a message is filtered out, it does not get processed further.
   *
   * @private
   * @param {DiscordMessage} message
   * @memberof Bot
   */
  private async handleIncoming(message: DiscordMessage) {
    for (const filterService of this.filters) {
      if (await filterService.filter(message)) {
        this.logger.info('Message was not filtered, passing further.', {
          messageId: message.id,
          filter: filterService.name,
        });

        this.filtered.next(message);
      } else {
        this.logger.info('Message was filtered out.', {
          messageId: message.id,
          filter: filterService.name,
        });
      }
    }
  }

  /**
   * Handle messages that have been passed through the filters by trying to
   * match them with available commands. If the matcher produces Command.None,
   * it means that the message should not be passed for further processing.
   *
   * @private
   * @param {DiscordMessage} message
   * @returns
   * @memberof Bot
   */
  private async handleFiltered(message: DiscordMessage) {
    const [command, action] = await this.matcher.match(message);
    if (command === Command.None) {
      this.logger.info('No commands have been matched.', { messageId: message.id });
      return;
    }

    this.logger.info('Message matched!', { messageId: message.id, command, action });
    this.matched.next({ command, action, message });
  }

  /**
   * Handle messages that have been matched to commands and actions
   * by trying to parse them with handlers.
   *
   * Only one handler will pick up a message, and in case of an incorrect
   * configuration where more than one handler matches a command, the frist one
   * will be used.
   *
   * @private
   * @param {MatchedMessage} context matched message context
   * @returns
   * @memberof Bot
   */
  private async handleMatched(context: MatchedMessage) {
    for (const handlerService of this.handlers) {
      if (await handlerService.check(context)) {
        this.logger.info('Command matched!', {
          messageId: context.message.id,
          command: context.command,
          action: context.action,
          handler: handlerService.name,
        });

        const responseContext = await handlerService.handle(context);
        if (responseContext.command === Command.None) {
          this.logger.info('Handler did not produce additional commands.', {
            messageId: context.message.id,
            handler: handlerService.name,
          });

          return;
        }

        this.logger.info('Handler produced an additional command, processing further.', {
          messageId: context.message.id,
          command: context.command,
          action: context.action,
          newCommand: responseContext.command,
          newAction: responseContext.action,
          handler: handlerService.name,
        });

        this.matched.next(responseContext);
      }

      this.logger.info('Command has not been handled.', {
        messageId: context.message.id,
        command: context.command,
        action: context.action,
        handler: handlerService.name,
      });
    }
  }

  /**
   * Starts the bot service, attaches event listeners and attempts
   * to log in to the discord client.
   *
   * @memberof Bot
   */
  public async start() {
    this.logger.info('Starting bot service.');

    const streamError = (error: Error) => {
      this.logger.error('Bot did not handle error.', { error });
    }

    this.incoming.subscribe((next) => this.handleIncoming(next).catch(streamError));
    this.filtered.subscribe((next) => this.handleFiltered(next).catch(streamError))
    this.matched.subscribe((next) => this.handleMatched(next).catch(streamError));

    this.clientController.attachHandler('ready', () => this.logger.info('Bot started, discord listener is ready.'));
    this.clientController.attachHandler('message', (message: DiscordMessage) => this.incoming.next(message));

    await this.clientController.login(this.config.token);
    await this.clientController.setPresence({ status: 'online', activity: { name: 'omnireborn.io' } });
  }

  /**
   * Stops the bot service, completes queues and removes listeners.
   * Finally, destroys the client.
   * @memberof Bot
   */
  public async stop() {
    this.logger.info('Stopping bog service, completing queues.');
    this.incoming.complete();
    this.filtered.complete();
    this.matched.complete();

    this.logger.info('Queues complete, removing listeners.');
    this.clientController.removeAllListeners(['ready', 'message']);
    this.clientController.destroy();

    this.logger.info('Listeners removed, shutting down.');
  }
}
