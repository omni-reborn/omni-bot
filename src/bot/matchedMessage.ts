import { Command, Action } from 'src/constants';
import { Message as DiscordMessage } from 'discord.js';

export interface MatchedMessage {
  command: Command;
  action: Action;
  message: DiscordMessage;
}
