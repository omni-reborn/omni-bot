import { IPrefixRepository } from 'src/repository';
import { REPOSITORY_IDENTIIFER, SERVICE_IDENTIFIER } from 'src/constants';
import { inject } from 'inversify';
import { ILogger } from 'src/logger';
import { DatabaseError } from 'src/error/database';
import { exists, Optional } from 'src/util';
import { Prefix } from 'src/entity';

export interface IPrefixController {
  getByServerId(serverId: string): Promise<Optional<Prefix>>;
}

export class PrefixController implements IPrefixController {
  private readonly logger: ILogger;
  private readonly prefixRepository: IPrefixRepository;

  public constructor(
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
    @inject(REPOSITORY_IDENTIIFER.IPrefixRepository) prefixRepository: IPrefixRepository,
  ) {
    this.logger = logger;
    this.prefixRepository = prefixRepository;
  }

  /**
   * Tries to obtain a prefix given a server id.
   * Throws a database error when applicable.
   *
   * Results are cached.
   * @param {string} serverId
   * @returns
   * @memberof PrefixController
   */
  public async getByServerId(serverId: string) {
    try {
      return await this.prefixRepository.findOne({ serverId }, { cache: true });
    } catch (error) {
      this.logger.error('Failed to retrieve prefix from the database', error);
      throw new DatabaseError('Failed to retrieve prefix from the database', error);
    }
  }
}
