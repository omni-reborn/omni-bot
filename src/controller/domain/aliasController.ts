import { REPOSITORY_IDENTIIFER, SERVICE_IDENTIFIER, Command } from 'src/constants';
import { IAliasRepository } from 'src/repository';
import { Alias } from 'src/entity';
import { inject } from 'inversify';
import { ILogger } from 'src/logger';
import { DatabaseError } from 'src/error/database';
import { Optional } from 'src/util';

export interface IAliasController {
  getByCommandName(command: Command): Promise<Optional<Alias>>;
  getByValueAndServerId(value: string, serverId: string): Promise<Optional<Alias>>;
  getGlobalByValue(value: string): Promise<Optional<Alias>>;
}

export class AliasController implements IAliasController {
  private readonly logger: ILogger;
  private readonly aliasRepository: IAliasRepository;

  public constructor(
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
    @inject(REPOSITORY_IDENTIIFER.IAliasRepository) aliasRepository: IAliasRepository,
  ) {
    this.logger = logger;
    this.aliasRepository = aliasRepository;
  }

  /**
   * Tries to obtain an alias given a command name.
   * Throws a database error when applicable.
   *
   * Results are cached.
   * @param {Command} command
   * @returns
   * @memberof AliasController
   */
  public async getByCommandName(command: Command) {
    try {
      return await this.aliasRepository.findOne({ command }, { cache: true });
    } catch (error) {
      this.logger.error('Failed to retrieve alias from the database', error);
      throw new DatabaseError('Failed to retrieve alias from the database', error);
    }
  }

  /**
   * Tries to obtain an alias given a value and a server id.
   * Throws a database error when applicable.
   *
   * Results are cached.
   * @param {string} value
   * @param {string} serverId
   * @returns
   * @memberof AliasController
   */
  public async getByValueAndServerId(value: string, serverId: string) {
    try {
      return await this.aliasRepository.findOne({ value, serverId }, { cache: true });
    } catch (error) {
      this.logger.error('Failed to retrieve alias from the database', error);
      throw new DatabaseError('Failed to retrieve alias from the database', error);
    }
  }

  /**
   * Tries to obtain a global alias given a value.
   * Throws a database error when applicable.
   *
   * Results are cached.
   * @param {string} value
   * @returns
   * @memberof AliasController
   */
  public async getGlobalByValue(value: string) {
    try {
      return await this.aliasRepository.findOne({ value, serverId: undefined }, { cache: true });
    } catch (error) {
      this.logger.error('Failed to retrieve alias from the database', error);
      throw new DatabaseError('Failed to retrieve alias from the database', error);
    }
  }
}
