import { injectable, inject } from 'inversify';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { ILoggerConfig } from 'src/config/logger';

export enum LogLevel {
  Info = 'info',
  Warn = 'warning',
  Error = 'error',
}

export interface LogOptions {
  isTimestamp: boolean;
  isLevel: boolean;
}

export interface ILogger {
  info(message: string, data?: any, options?: LogOptions): void;
  warn(message: string, data?: any, options?: LogOptions): void;
  error(message: string, data?: any, options?: LogOptions): void;
}

@injectable()
export class Logger implements ILogger {
  private readonly config: ILoggerConfig;
  private readonly options: LogOptions;

  public constructor(@inject(CONFIG_IDENTIFIER.ILoggerConfig) config: ILoggerConfig) {
    this.config = config;
    this.options = {
      isTimestamp: config.defaults.timestamp,
      isLevel: config.defaults.level,
    };
  }

  private formatMessage(message: string, data: any, level: LogLevel, options: LogOptions) {
    let formattedMessage = { message, ...data };

    formattedMessage = options.isTimestamp ? { ...formattedMessage, ...{ timestamp: new Date() } } : formattedMessage;
    formattedMessage = options.isLevel ? { ...formattedMessage, ...{ level } } : formattedMessage;
    return JSON.stringify(formattedMessage);
  }

  private logMessage(message: string, data: any, level: LogLevel, options?: LogOptions) {
    const messageOptions: LogOptions = { ...this.options, ...options };
    const outMessage = this.formatMessage(message, data, level, messageOptions);
    process.stdout.write(outMessage);
  }

  public info(message: string, data?: any, options?: LogOptions) {
    this.logMessage(message, data, LogLevel.Info, options);
  }

  public warn(message: string, data?: any, options?: LogOptions) {
    this.logMessage(message, data, LogLevel.Warn, options);
  }

  public error(message: string, data?: any, options?: LogOptions) {
    this.logMessage(message, data, LogLevel.Error, options);
  }
}
