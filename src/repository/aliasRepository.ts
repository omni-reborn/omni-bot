import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Alias } from 'src/entity';

export type IAliasRepository = BaseRepository<Alias>;

@EntityRepository(Alias)
export class AliasRepository extends BaseRepository<Alias> implements IAliasRepository { }
