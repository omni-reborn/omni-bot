import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Prefix } from 'src/entity';

export type IPrefixRepository = BaseRepository<Prefix>;

@EntityRepository(Prefix)
export class PrefixRepository extends BaseRepository<Prefix> implements IPrefixRepository { }
