export const SERVICE_IDENTIFIER = {
  DiscordClient: Symbol.for('DiscordClient'),
  IDiscordClientController: Symbol.for('IDiscordClientController'),
  IDatabaseConnector: Symbol.for('IDatabaseConnector'),
  ILogger: Symbol.for('ILogger'),
  IBot: Symbol.for('IBot'),
  IFilter: Symbol.for('IFilter'),
  IMatcher: Symbol.for('IMatcher'),
  IHandler: Symbol.for('IHandler'),
};
