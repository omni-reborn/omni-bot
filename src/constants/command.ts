/**
 * This enum defines base commands matchable by the matcher and handlers.
 * Entry values represent real, core commands, in form in which they should
 * appear when invoked in discord.
 *
 * These commands can be changed, but do mind that handlers will be matching
 * against them using their config-defined commands, as options for those
 * are dynamically inferred and validated against.
 * These are also used by the Alias entity in its' command column, so bear
 * in mind that changing any of these values will cause old, stored aliases
 * to not function properly.
 * Whenever preexisting values are changed, a migration should be issued.
 *
 * WARNING!
 * This enum has to have ONLY string values! Adding fields with
 * default (numeric) values can cause unexpected behavior.
 *
 * Additionally, command "None" has to be present and is treated
 * as a reserved keyword with special behavior.
 */
export enum Command {
  Echo = 'echo',
  None = 'none',
};
