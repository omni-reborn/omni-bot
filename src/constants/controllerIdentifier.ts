export const CONTROLLER_IDENTIFIER = {
  IAliasController: Symbol.for('IAliasController'),
  IPrefixController: Symbol.for('IPrefixController'),
}
