/**
 * This enum identifies base actions matchable by handlers.
 *
 * While these actions can be changed, do bear in mind that
 * they are used by the Alias entity and modifying any of them
 * will cause database-commited aliases with those actions to
 * not function properly until updated.
 *
 * Whenever preexisting values are changed, a migration should be issued.
 */
export enum Action {
  Execute = 'execute',
  Help = 'help',
  None = 'none',
}
