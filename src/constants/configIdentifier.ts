export const CONFIG_IDENTIFIER = {
  IBotConfig: Symbol.for('IBotConfig'),
  ILoggerConfig: Symbol.for('ILoggerConfig'),
  IAppServiceConfig: Symbol.for('IAppServiceConfig'),
  IMatcherConfig: Symbol.for('IMatcherConfig'),
  IDatabaseConnectorConfig: Symbol.for('IDatabaseConnectorConfig'),
}
