import * as uuid from 'uuid/v4';
import { Column, PrimaryColumn, Entity } from 'typeorm';
import { exists } from 'src/util';
import { Command, Action } from 'src/constants';

export interface CreateAliasProps {
  id?: string;
  serverId?: string;
  command: Command;
  action: Action;
  value: string;
}

@Entity({ name: 'Alias' })
export class Alias {
  @PrimaryColumn('name')
  public id: string;

  @Column()
  public serverId?: string;

  @Column({ nullable: false })
  public command: Command;

  @Column({ nullable: false })
  public action: Action;

  @Column({ nullable: true })
  public value: string;

  public static create(props: CreateAliasProps) {
    const alias = new Alias();

    alias.id = exists(props.id) ? props.id : uuid();
    alias.serverId = props.serverId;
    alias.command = props.command;
    alias.action = props.action;
    alias.value = props.value;

    return alias;
  }
}
