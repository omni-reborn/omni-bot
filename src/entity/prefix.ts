import * as uuid from 'uuid/v4';
import { Column, PrimaryColumn, Entity } from 'typeorm';
import { exists } from 'src/util';

export interface CreatePrefixProps {
  id?: string;
  serverId: string;
  value: string;
}

@Entity({ name: 'Prefix' })
export class Prefix {
  @PrimaryColumn('uuid')
  public id: string;

  @Column({ nullable: false, unique: true })
  public serverId: string;

  @Column({ nullable: false })
  public value: string;

  public static create(props: CreatePrefixProps) {
    const prefix = new Prefix();

    prefix.id = exists(props.id) ? props.id : uuid();
    prefix.serverId = props.serverId;
    prefix.value = props.value;

    return prefix;
  }
}
