import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TLoggerConfig>;
export interface ILoggerConfig extends T { }

// Since io-ts does not support enums (yet),
// make sure that level always mirrors the LogLevel enum
export const TDefaults = t.type({
  timestamp: t.boolean,
  level: t.boolean,
});

export const TLoggerConfig = t.type({
  defaults: TDefaults,
});
