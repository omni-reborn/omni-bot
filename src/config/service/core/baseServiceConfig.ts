import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TBaseServiceConfig>;
export interface IBaseServiceConfig extends T { }

export const TBaseServiceConfig = t.type({
  name: t.string,
});
