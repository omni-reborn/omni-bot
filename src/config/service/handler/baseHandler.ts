import * as t from 'io-ts';
import { TBaseServiceConfig } from 'src/config/service/core';
import { createEnum } from 'src/config/core/io-enum';
import { Command, Action } from 'src/constants';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TBaseHandlerConfig>;
export interface IBaseHandlerConfig extends T { }

const commandType = createEnum<Command>(Command, 'Command');
const actionType = createEnum<Action>(Action, 'Action');

const TBaseProps = t.type({
  command: commandType,
  actions: t.array(actionType),
})

export const TBaseHandlerConfig = t.intersection([
  TBaseServiceConfig,
  TBaseProps,
]);

