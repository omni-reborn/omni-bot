import * as t from 'io-ts';
import { TBaseServiceConfig } from 'src/config/service/core';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TBaseFilterConfig>;
export interface IBaseFilterConfig extends T { }

export const TBaseFilterConfig = TBaseServiceConfig;
