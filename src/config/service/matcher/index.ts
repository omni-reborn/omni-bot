import * as t from 'io-ts';
import { TBaseServiceConfig } from 'src/config/service/core';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TMatcherConfig>;
export interface IMatcherConfig extends T { }

const TMatcherDefaults = t.type({
  defaults: t.type({
    prefix: t.string,
  }),
})

export const TMatcherConfig = t.intersection([
  TBaseServiceConfig,
  TMatcherDefaults,
]);
