import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TAppServiceConfig>;
export interface IAppServiceConfig extends T { }

export const TAppServiceConfig = t.type({
  serviceName: t.string,
  port: t.string,
});
