import 'reflect-metadata';
import { Container } from 'inversify';
import { configModule } from './configModule';
import { databaseModule } from './databaseModule';
import { serviceModule } from './serviceModule';

export async function installContainer(): Promise<Container> {
  const container = new Container();
  container.load(configModule);
  container.load(databaseModule);
  container.load(serviceModule);

  return container;
}
