import { SERVICE_IDENTIFIER } from 'src/constants';
import { DatabaseConnector } from 'src/database';
import { interfaces, ContainerModule } from 'inversify';

export const databaseModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<DatabaseConnector>(SERVICE_IDENTIFIER.IDatabaseConnector)
      .to(DatabaseConnector)
      .inSingletonScope();
  },
);
