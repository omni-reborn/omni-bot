import { SERVICE_IDENTIFIER } from 'src/constants';
import { interfaces, ContainerModule } from 'inversify';
import { ILogger, Logger } from 'src/logger';
import { Client as DiscordClient } from 'discord.js';
import { IBot, Bot } from 'src/bot';

export const serviceModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<ILogger>(SERVICE_IDENTIFIER.ILogger)
      .to(Logger)
      .inSingletonScope();

    bind<IBot>(SERVICE_IDENTIFIER.IBot)
      .to(Bot)
      .inSingletonScope();

    bind<DiscordClient>(SERVICE_IDENTIFIER.DiscordClient)
      .toConstantValue(new DiscordClient());
  },
);
