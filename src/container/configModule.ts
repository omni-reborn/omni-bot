import { ContainerModule, interfaces } from 'inversify';
import { IDatabaseConnectorConfig, TDatabaseConnectorConfig } from 'src/config/database';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { loadConfig } from 'src/config/core';
import { ILoggerConfig, TLoggerConfig } from 'src/config/logger';
import { IAppServiceConfig, TAppServiceConfig } from 'src/config/app';

export const configModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IAppServiceConfig>(CONFIG_IDENTIFIER.IAppServiceConfig)
      .toConstantValue(loadConfig<IAppServiceConfig>(TAppServiceConfig, '/usr/src/app/config/appServiceConfig.yml'));

    bind<IDatabaseConnectorConfig>(CONFIG_IDENTIFIER.IDatabaseConnectorConfig)
      .toConstantValue(loadConfig<IDatabaseConnectorConfig>(TDatabaseConnectorConfig, '/usr/src/app/config/databaseConfig.yml'));

    bind<ILoggerConfig>(CONFIG_IDENTIFIER.ILoggerConfig)
      .toConstantValue(loadConfig<ILoggerConfig>(TLoggerConfig, '/usr/src/app/config/loggerConfig.yml'));
  },
);
