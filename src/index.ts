import 'reflect-metadata';
import { installContainer } from 'src/container/installer';
import { InversifyExpressServer } from 'inversify-express-utils';
import { CONFIG_IDENTIFIER, SERVICE_IDENTIFIER } from './constants';
import { initializeTransactionalContext } from 'typeorm-transactional-cls-hooked';
import { IAppServiceConfig } from './config/app';
import { IDatabaseConnector } from './database';
import { ILogger } from './logger';
import { IBot } from './bot';
import * as express from 'express';
import * as morgan from 'morgan';
import * as cors from 'cors';

// IMPORT EXPRESS CONTROLLERS - ONLY HERE - USE RELATIVE PATHS!

async function main() {
  try {
    const container = await installContainer();

    const bot = container.get<IBot>(SERVICE_IDENTIFIER.IBot);
    const logger = container.get<ILogger>(SERVICE_IDENTIFIER.ILogger);
    const appConfig = container.get<IAppServiceConfig>(CONFIG_IDENTIFIER.IAppServiceConfig);
    const databaseConnector = container.get<IDatabaseConnector>(SERVICE_IDENTIFIER.IDatabaseConnector);

    initializeTransactionalContext();

    const app = express();
    app.use(morgan('short'));
    app.use(cors({ origin: 'http://localhost:3001', credentials: true }));

    const server = new InversifyExpressServer(container, null, null, app);
    await databaseConnector.connect();

    const httpServer = server.build();
    httpServer.listen(appConfig.port);
    logger.info(`Server listening on port ${appConfig.port}.`);

    await bot.start();
    logger.info('Bot started.');

  } catch (error) {
    console.log(error);
  }
}

/* tslint:disable: no-floating-promises */
main();
