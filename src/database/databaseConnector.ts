import { inject, injectable } from 'inversify';
import { createConnection } from 'typeorm';
import { IDatabaseConnectorConfig } from 'src/config/database';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { DatabaseConnectionError, DatabaseRepeatConnectionError } from 'src/error/database';

export interface IDatabaseConnector {
  connect(): Promise<void>;
}

@injectable()
export class DatabaseConnector implements IDatabaseConnector {
  private readonly config: IDatabaseConnectorConfig;
  private connectionCreated: boolean;

  public constructor(@inject(CONFIG_IDENTIFIER.IDatabaseConnectorConfig) config: IDatabaseConnectorConfig) {
    this.config = config;
    this.connectionCreated = false;
  }

  public async connect() {
    if (this.connectionCreated) {
      throw new DatabaseRepeatConnectionError();
    }

    try {
      await createConnection({
        type: this.config.type,
        host: this.config.host,
        port: parseInt(this.config.port, 10),
        username: this.config.username,
        password: this.config.password,
        database: this.config.database,
        entities: [],
        cache: {
          duration: 60000,
        },
      });
      this.connectionCreated = true;
    } catch (error) {
      throw new DatabaseConnectionError('Could not establish connection with the database', error);
    }
  }
}
