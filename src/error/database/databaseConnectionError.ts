import { DatabaseError } from './databaseError';

export class DatabaseConnectionError extends DatabaseError {
  public constructor(message = 'Could not establish connection with the database', error?: any) {
    super(message, error);
  }
}
