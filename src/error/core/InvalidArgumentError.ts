import { BaseError } from './baseError';

export class InvalidArgumentError extends BaseError {
  public constructor(message = 'Invalid argument error.', error?: any) {
    super(message, error);
  }
}
