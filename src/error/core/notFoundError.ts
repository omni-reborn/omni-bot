import { BaseError } from './baseError';

export class NotFoundError extends BaseError {
  public constructor(message = 'Target not found.', error?: any) {
    super(message, error);
  }
}
