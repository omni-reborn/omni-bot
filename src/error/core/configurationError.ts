import { BaseError } from './baseError';

export class ConfigurationError extends BaseError {
  public constructor(message = 'Invalid configuration.', error?: any) {
    super(message, error);
  }
}
