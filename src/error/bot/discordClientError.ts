import { BaseError } from 'src/error/core';

export class DiscordClientError extends BaseError {
  public constructor(message = 'Discord client error.', error?: any) {
    super(message, error);
  }
}
