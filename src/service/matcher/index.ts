import { SERVICE_IDENTIFIER, CONFIG_IDENTIFIER, CONTROLLER_IDENTIFIER, Command, Action } from 'src/constants';
import { Message as DiscordMessage } from 'discord.js';
import { IService, BaseService } from '../core';
import { ILogger } from 'src/logger';
import { IAliasController, IPrefixController } from 'src/controller/domain';
import { IMatcherConfig } from 'src/config/service/matcher';
import { inject } from 'inversify';
import { exists, mustExist } from 'src/util';

export interface IMatcher extends IService {
  match(message: DiscordMessage): Promise<[Command, Action]>;
}

/**
 * A matcher's purpose is to match messages to commands through it's sole 'match()' method.
 * This service's config contains default values to be used in case none have been defined
 * for a specific discord guild.
 *
 * @export
 * @class Matcher
 * @extends {BaseService}
 * @implements {IMatcher}
 */
export class Matcher extends BaseService implements IMatcher {
  private readonly logger: ILogger;
  protected readonly config: IMatcherConfig;
  private readonly aliasController: IAliasController;
  private readonly prefixController: IPrefixController;

  public constructor(
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
    @inject(CONFIG_IDENTIFIER.IMatcherConfig) config: IMatcherConfig,
    @inject(CONTROLLER_IDENTIFIER.IAliasController) aliasController: IAliasController,
    @inject(CONTROLLER_IDENTIFIER.IPrefixController) prefixController: IPrefixController,
  ) {
    super(config);

    this.logger = logger;
    this.config = config;
    this.aliasController = aliasController;
    this.prefixController = prefixController;
  }

  /**
   * Tries to match a discord message to a command and an action.
   * Checks for prefixes configured in for the guild, with default
   * set in the matcher options.
   * Checks against both base commands and guild-configured aliases.
   *
   * The returned Action verb is always `Execute`, unless specified otherwise
   * in a matched alias.
   *
   * On no match, the Command returned is `None`.
   *
   * @param {DiscordMessage} message discord message
   * @returns {Promise<[Command, Action]>} a tuple of Command and Action
   * @memberof Matcher
   */
  public async match(message: DiscordMessage): Promise<[Command, Action]> {
    const storedPrefix = exists(message.guild) ? await this.prefixController.getByServerId(message.guild.id) : undefined;
    const prefix = exists(storedPrefix) ? storedPrefix.value : this.config.defaults.prefix;

    if (message.content.startsWith(prefix)) {
      const commandPhraseWithPrefix = mustExist(message.content.split(' ').shift());
      if (commandPhraseWithPrefix.length === prefix.length) {
        return [Command.None, Action.None];
      }

      const commandPhrase = commandPhraseWithPrefix.substring(prefix.length);
      for (const [, command] of Object.entries(Command)) {
        if (commandPhrase === command) {
          return [command, Action.Execute];
        }
      }

      const alias = exists(message.guild)
        ? await this.aliasController.getByValueAndServerId(commandPhrase, message.guild.id)
        : await this.aliasController.getGlobalByValue(commandPhrase);
      if (exists(alias)) {
        return [alias.command, alias.action];
      }
    }

    return [Command.None, Action.None];
  }
}
