import { IBaseServiceConfig } from 'src/config/service/core';
import { injectable, unmanaged } from 'inversify';

export interface IService {
  name: string;
}

@injectable()
export abstract class BaseService implements IService {
  protected config: IBaseServiceConfig;

  public constructor(@unmanaged() config: IBaseServiceConfig) {
    this.config = config;
  }

  public get name() {
    return this.config.name;
  }
}
