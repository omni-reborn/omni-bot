import { IService, BaseService } from 'src/service/core';
import { injectable, unmanaged } from 'inversify';
import { MatchedMessage } from 'src/bot';
import { IBaseHandlerConfig } from 'src/config/service/handler';
import { Action, Command } from 'src/constants';
import { ILogger } from 'src/logger';

export interface IHandler extends IService {
  check(context: MatchedMessage): Promise<boolean>;
  handle(context: MatchedMessage): Promise<MatchedMessage>;
}

/**
 * Base handler class that all handlers inherit from.
 * Defines the behavior of the check and handle methods.
 *
 * @export
 * @abstract
 * @class BaseHandler
 * @extends {BaseService}
 * @implements {IHandler}
 */
@injectable()
export abstract class BaseHandler extends BaseService implements IHandler {
  private readonly logger: ILogger;
  protected config: IBaseHandlerConfig;

  public constructor(
    @unmanaged() logger: ILogger,
    @unmanaged() config: IBaseHandlerConfig,
  ) {
    super(config);
    this.logger = logger;
  }

  /**
   * Checks if the handler matches the command and action by checking against the
   * handler config. Besides from having to match the command, the handler
   * has to be able to support a specific action.
   *
   * @param {MatchedMessage} context
   * @returns
   * @memberof BaseHandler
   */
  public async check(context: MatchedMessage) {
    if (this.config.command === context.command) {
      if (this.config.actions.includes(context.action)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Handles the message by invoking the correct handler based on the action passed.
   * A check() should always be ran before handle() is, although a failsafe mechanism
   * is in place to prevent unexpected results;
   *
   * @param {MatchedMessage} context
   * @returns
   * @memberof BaseHandler
   */
  public async handle(context: MatchedMessage) {
    switch (context.action) {
      case Action.Execute:
        return this.execute(context);
      case Action.Help:
        return this.help(context);
      default:
        this.logger.warn('None of the actions matched the handler config. \
          Make sure the config is correct and use check() to validate if the \
          context can be handled.');

        return {
          command: Command.None,
          action: Action.None,
          message: context.message,
        }
    }
  }

  protected abstract async execute(context: MatchedMessage): Promise<MatchedMessage>;
  protected abstract async help(context: MatchedMessage): Promise<MatchedMessage>;
}
